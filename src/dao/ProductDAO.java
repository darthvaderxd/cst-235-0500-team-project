/*
	Created By: Richard Williams
	Created: 12/5/2020
	Last Update Time: 12/6/2020
	Last Updated by: Donald Trowbridge
	Notes: 
		12/5/2020 : Created Product DAO
		12/6/2020 : Added search function (getProductsBySearchString()) that will find products whose names contain the submitted partial string. Example: A search on "gold" returns "gold plated cat widget"
*/

package dao;

import java.util.List;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import entities.Product;


/**
 * @author Richard Williamson
 *
 */
@Stateless
@LocalBean
@TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class ProductDAO {
	@PersistenceContext(unitName="catwidgets")
	private EntityManager em;
	
	@Resource
	UserTransaction utx;

	
	public void close() {
		em.close();
	}
	
	public void beginTransaction() {
		try {
			utx.begin();
		} catch (Exception e) {
			System.out.println("Transaction failed to begin " + e.getMessage());
		}
	}
	
	public void transactionRollback() {
		try {
			utx.rollback();
		} catch (Exception e) {
			System.out.println("error rolling back");
		}
	}
	
	public void commitTransaction() {
		try {
			utx.commit();
		} catch (Exception e) {
			System.out.println("Commit failed to begin " + e.getMessage());
		}
	}
	
	public void saveProduct(Product product) {
		try {
			beginTransaction();
			System.out.println("product id => " + product.getProductId());
			if (product.getProductId() < 1) {
				em.persist(product);
			} else {
				em.merge(product);
			}
			commitTransaction();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			transactionRollback();
		}
	}
	
	public void deleteProduct(Product product) {
		try {
			utx.begin();
			em.remove(em.contains(product) ? product : em.merge(product));
			utx.commit();
			em.getEntityManagerFactory().getCache().evict(Product.class);
		} catch (Exception e) {
			System.out.println("Error deleting productId => " + product.getProductId() + "\n" + e.getMessage());
			transactionRollback();
		}
	}
	
	public Product findProdcutById(long id) {
		try {
			return em.find(Product.class, id);
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<Product> getProducts() {
		// TODO: I do not know why the below is failing...
		// users = em.createNamedQuery("User.findAll", User.class ).getResultList();
		return em.createQuery("SELECT u from Product u", Product.class ).getResultList();
	}
	
	/*
	 * Gets Products based on partial name match
	 */
	public List<Product> getProductsBySearchString(String searchString){
		return em.createQuery("SELECT u from Product u where product_name like :name", Product.class).setParameter("name", "%" +searchString + "%").getResultList();
	}
	
	public Product getProductByName(String productName) {
		return em.createQuery("SELECT u from Product u where product_name like :name", Product.class).setParameter("name", productName).getSingleResult();
	}
}

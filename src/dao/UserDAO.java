/*
	Created By: Richard Williams
	Created: 12/5/2020
	Last Update Time: 12/18/2020
	Last Updated by: Donald Trowbridge
	Notes:
		12/5/2020 : Created User DAO
		12/18/2020 : Created getUsers() method. Get's current users from database.
*/

package dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import entities.User;


/**
 * @author Richard Williamson
 *
 */
@Stateless
@LocalBean
@TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class UserDAO {
	@PersistenceContext(unitName="catwidgets")
	private EntityManager em;
	
	@Resource
	UserTransaction utx;

	private List<User> users = new ArrayList<User>();

	@PostConstruct
	public void init() {
		try {
			// TODO: I do not know why the below is failing...
			// users = em.createNamedQuery("User.findAll", User.class ).getResultList();
			users = em.createQuery("SELECT u from User u", User.class ).getResultList();
		} catch (Exception e) {
			System.out.println("Error => " + e.getMessage());
		}
	}
	
	public void close() {
		em.close();
	}
	
	public void beginTransaction() {
		try {
			utx.begin();
		} catch (Exception e) {
			System.out.println("Transaction failed to begin " + e.getMessage());
		}
	}
	
	public void transactionRollback() {
		try {
			utx.rollback();
		} catch (Exception e) {
			System.out.println("error rolling back");
		}
	}
	
	public void commitTransaction() {
		try {
			utx.commit();
		} catch (Exception e) {
			System.out.println("Commit failed to begin " + e.getMessage());
		}
	}
	
	public void saveUser(User user) {
		try {
			beginTransaction();
			System.out.println("user id => " + user.getUserId());
			if (user.getUserId() < 1) {
				em.persist(user);
			} else {
				em.merge(user);
			}
			commitTransaction();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			transactionRollback();
		}
		users.add(user);
	}
	
	public void deleteUser(User user) {
		try {
			utx.begin();
			em.remove(em.contains(user) ? user : em.merge(user));
			utx.commit();
		} catch (Exception e) {
			transactionRollback();
		}
	}
	
	public User findUserByUsername(String username) {
		try {
			String sql  = "SELECT u FROM User u WHERE u.username = ?1";
			return (User) em.createQuery(sql)
					.setParameter(1, username)
					.getSingleResult();
		} catch (Exception e) {
			// could be user not found
			return null;
		}
	}
	
	public List<User> getUsers() {
		return em.createQuery("SELECT u FROM User u", User.class).getResultList();
	}
	
	
}

/*
	Created By: Richard Williams
	Created: 11/28/2020
	Last Update Time: 12/6/2020
	Last Updated by: Donald Trowbridge
	Notes:
		-11/28/2020 : Created product interface
		-12/5/2020  : Updated product interface to edit product
		-12/6/2020  : Added function to search by partial product name
*/

package business;

// Written by: Richard Williamson

import java.util.List;
import javax.ejb.Local;
import entities.Product;

@Local
public interface ProductServiceInterface {
	public List<Product> getProducts();
	public void addProduct(Product product);
	public void removeProduct(Product product);
	public void saveProduct(Product product);
	public Product getProductById(long id);
	public List<Product> getProductsBySearchString(String searchString);
	public Product getProductByName(String productName);
}

/*
	Created By: Richard Williams
	Created: 11/28/2020
	Last Update Time: 12/6/2020
	Last Updated by: Donald Trowbridge
	Notes:
		-11/28/2020 : Created product bean
		-12/5/2020  : Updated product bean to edit product
		-12/6/2020  : Added function to search by partial product name
*/

package business;

// Written by: Richard Williamson

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import dao.ProductDAO;
import entities.Product;

/**
 * Session Bean implementation class ProductBusinessService
 */
@Stateless
@LocalBean
@Alternative
public class ProductBusinessService implements ProductServiceInterface {
	@EJB
	ProductDAO service;

	public List<Product> getProducts() {
		return service.getProducts();
	}
	
	public void addProduct(Product product) {
		service.saveProduct(product);
	}
	
	public void removeProduct(Product product) {
		service.deleteProduct(product);
	}

	public void saveProduct(Product product) {
		service.saveProduct(product);
	}
	
	public Product getProductById(long id) {
		return service.findProdcutById(id);
	}
	
	public List<Product> getProductsBySearchString(String searchString){
		return service.getProductsBySearchString(searchString);
	}
	
	public Product getProductByName(String productName) {
		return service.getProductByName(productName);
	}
}

/*
 	Created By: Richard Williams
	Created: 11/28/2020
	Last Update Time: 12/5/2020
	Last Updated by: Richard Williams
	Notes:
		-11/28/2020 : Created User Enterperise bean
		-12/5/2020  : Added JPA for functions
*/


package business;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import java.util.ArrayList;
import java.util.List;

import dao.UserDAO;
import entities.User;

/**
 * Session Bean implementation class UserBusinessService
 */
@Stateless
@LocalBean
public class UserBusinessService {
	@EJB
	UserDAO service;

	private List<User> users = new ArrayList<User>();

	@PostConstruct
	public void init() {
    	users = service.getUsers(); 
    }

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public boolean addUser(User user) {
		User foundUser = findUserByUserName(user.getUsername());
		if (foundUser == null) {
			service.saveUser(user);
			System.out.println("Adding user " + user.getUsername() + " total users is " + users.size());
			return true;
		}
		return false;
	}
	
	public void saveUser(User user) {
		service.saveUser(user);
	}
	
	public void removeUser(User user) {
		users.remove(user);
	}
	
	public User findUserByUserName(String username) {
		return service.findUserByUsername(username);
	}
	
	public boolean validateLogin(String username, String password) {
		User user = findUserByUserName(username);
		if (user != null) {
			return user.getUsername().equals(username) && user.getPassword().equals(password);
		}
		return false;
	}
}

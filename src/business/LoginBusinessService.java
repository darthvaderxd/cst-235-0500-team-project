package business;

import java.io.IOException;
import java.security.Principal;
import java.security.acl.Group;
import java.util.Map;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;

import org.jboss.security.auth.spi.AbstractServerLoginModule;

public class LoginBusinessService extends AbstractServerLoginModule {
    @Inject
	private UserBusinessService service;
    private java.security.Principal identity;

    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
        System.out.println("JPALoginModule.initialize()");
        super.initialize(subject, callbackHandler, sharedState, options);
        InitialContext context = null;
        try {
            context = new InitialContext();
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public boolean login() throws LoginException {
        System.out.println("JPALoginModule.login()");
        try {
            // Setup default callback handlers.
            Callback[] callbacks = new Callback[] { new NameCallback("Username: "), new PasswordCallback("Password: ", false) };

            callbackHandler.handle(callbacks);

            String username = ((NameCallback) callbacks[0]).getName();
            String password = new String(((PasswordCallback) callbacks[1]).getPassword());

            if (service.validateLogin(username, password)) {
                throw new LoginException("Authentication Failed: Wrong Password");
            }
            try {
                identity = createIdentity(username);
            } catch (Exception e) {
                throw new LoginException("Unable to Create Identity");
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedCallbackException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            System.out.println(e.getMessage()+" "+e.getLocalizedMessage());
        }
        return false;
    }

    @Override
    public boolean commit() throws LoginException {
        if (identity != null) {
            if (subject.isReadOnly()) {
                throw new LoginException("subject is read only");
            }
        }
        return super.commit();
    }

    @Override
    public boolean abort() throws LoginException {
        logout();
        return true;
    }

    @Override
    public boolean logout() throws LoginException {
        return super.logout();
    }

    @Override
    protected Principal getIdentity() {
        return identity;
    }

    @Override
    protected Group[] getRoleSets() throws LoginException {
        return new Group[0];
    }
}
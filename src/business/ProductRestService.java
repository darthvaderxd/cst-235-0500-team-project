package business;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import entities.Product;

import java.net.URI;
import java.util.List;
import javax.inject.Inject;

@RequestScoped
@Path("/products")
@Produces("application/json")
@Consumes("application/json")
public class ProductRestService {
	@Inject ProductServiceInterface products;
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAllProductsJson(){
		try {
			return products.getProducts();
		} catch(Exception e) {
			throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GET
	@Path("/product/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Product getProductById(@PathParam("id") int productId) {
		try {
		Long id = Integer.toUnsignedLong(productId);
		return products.getProductById(id);
		}catch(Exception e) {
			throw new WebApplicationException(e, Response.Status.NOT_FOUND);
		}
	}
	
	@GET
	@Path("/find/by-name/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Product getProductByName(@PathParam("name") String productName) {
		try {
			return products.getProductByName(productName);
		} catch(Exception e) {
			throw new WebApplicationException(e, Response.Status.NOT_FOUND);
		}
	}
	
	@GET
	@Path("/find/by-name-like/{namelike}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductBySearchString(@PathParam("namelike") String searchString){
		try{
			return products.getProductsBySearchString(searchString);
		}catch(Exception e){
			throw new WebApplicationException(e, Response.Status.NOT_FOUND);
		}
	}
	
	@POST
	@Path("/product")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createProduct(Product product) {
		try {
			products.saveProduct(product);
			Product newProduct = products.getProductByName(product.getProductName());
			ResponseBuilder response = Response.created(URI.create("/products/product/" + newProduct.getProductId()));
			response.status(Status.CREATED);
			return response.build();
		}catch(Exception e){
			throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DELETE
	@Path("/product/{id}")
	public Response deleteProduct(@PathParam("id") int id) {
		try {
			Long productId = Integer.toUnsignedLong(id);
			Product product = products.getProductById(productId);
			products.removeProduct(product);
			return Response.status(200).build();
		} catch(Exception e) {
			throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PUT
	@Path("/product/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProduct(@PathParam("id") int id, Product product) {
		try {
			Long productId = Integer.toUnsignedLong(id);
			Product productToUpdate = products.getProductById(productId);
			productToUpdate.setProductName(product.getProductName());
			productToUpdate.setDescription(product.getDescription());
			productToUpdate.setCost(product.getCost());
			products.saveProduct(productToUpdate);
			ResponseBuilder response = Response.created(URI.create("/products/product/" + productToUpdate.getProductId()));
			response.status(Status.CREATED);
			return response.build();
		} catch (Exception e) {
			throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}
}

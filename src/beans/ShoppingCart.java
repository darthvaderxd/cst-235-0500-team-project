package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import entities.Product;

@ManagedBean(name = "ShoppingCart")
@SessionScoped
public class ShoppingCart {	
	List<Product> products = new ArrayList<Product>();

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	public List<Product> clearShoppingCart() {
		products.clear();
		return products;
	}
	
	public void addProduct(Product p) {
		products.add(p);
	}
	
	public void removeProduct(Product p) {
		products.remove(p);
	}
	
	public float getTotalCost() {
		float cost = 0;
		
		for (int i = 0; i < products.size(); i += 1) {
			cost += products.get(i).getCost();
		}
		
		return cost; 
	}
}

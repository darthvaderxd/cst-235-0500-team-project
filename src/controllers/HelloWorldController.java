package controllers;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import beans.HelloWorldBean;

@ManagedBean(name = "helloWorldController")
@ViewScoped
public class HelloWorldController implements  Serializable {
	private static final long serialVersionUID = 1L;
	private HelloWorldBean hwb;
	
	
	public HelloWorldBean getHwb() {
		return hwb;
	}

	public void setHwb(HelloWorldBean hwb) {
		this.hwb = hwb;
	}

	public String updateMessage(HelloWorldBean hwb) {
		this.hwb = hwb;
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getRequestMap().put("hwb", hwb);
		System.out.println("hwb => " + hwb.getMessage());
		return "HelloWorld.xhtml?faces-redirect=true";
	}
}

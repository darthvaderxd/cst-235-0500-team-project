/*
	Created By: Richard Williams
	Created: 11/28/2020
	Last Update Time: 12/6/2020
	Last Updated by: Donald Trowbridge
	Notes:
		-11/28/2020 : Created Product Controller
		-12/5/2020  : Added functions for CRUD invoke CRUD operations and direct to admin pages
		-12/6/2020  : Added function viewSearchProducts() and updated function getSearchProducts() to add a search bar feature to the admin and products report pages. Added function viewProduct() to display product details
*/

package controllers;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

//written by: Richard Williamson

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import beans.ShoppingCart;
import business.ProductServiceInterface;
import business.UserBusinessService;
import entities.Product;
import entities.User;

@ManagedBean(name="productController")
@SessionScoped
public class ProductController {
	@Inject
	protected ProductServiceInterface products;
	
	
	@EJB
	protected UserBusinessService userService;
	
	@Inject
	protected ShoppingCart shoppingCart;
	
	protected User user;
	protected Product newProduct;
	public Product product;
	public List<Product> searchProducts;
	
	@PostConstruct
	public void init() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			user = (User) context.getExternalContext().getRequestMap().get("User");
			if (user == null) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets/?faces-redirect=true");
			}
		} catch (Exception e) {
			System.out.println("error => " + e.getMessage());
			// do nothing
		}
	}
	
	public User getUser() {
		if (user == null) {
			try {
				FacesContext context = FacesContext.getCurrentInstance();
				user = (User) context.getExternalContext().getRequestMap().get("User");
			} catch (Exception e) {
				// do nothing..
			}
		}
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public Product getNewProduct() {
		return newProduct;
	}
	
	public void setNewProduct(Product product) {
		newProduct = product;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
		
	public ProductServiceInterface getService() {
		return products;
	}
	
	public void setService(ProductServiceInterface products) {
		this.products = products;
	}
	
	public List<Product> getShoppingCart() {
		return shoppingCart.getProducts();
	}
	
	/*
	 * Saves recently added product to database
	 */
	public String saveNewProduct(Product newProduct) {
		products.addProduct(newProduct);
		FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().put("newProduct", newProduct);
		return "/Admin/Product/ProductConfirmation.xhtml?faces-redirect=true";
	}
	
	/*
	 * Displays all search products if search bar is empty and returns a list of strings with similar names if search bar not empty
	 */
	public List<Product> getProducts() {
		//return products.getProducts();
		if(searchProducts == null) {
			return products.getProducts();
		} else {
			return searchProducts;
		}
	}
	
	/*
	 * Allows user to edit specific product.
	 */
	public String editProduct(long id) throws IOException {
		product = products.getProductById(id);
		if (product != null) {
			System.out.println("editProduct => " + product.getProductId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getRequestMap().put("Product", product);
		} else {
			System.out.println("Product not found");
		}
		
		return "/Admin/Product/EditProduct.xhtml?faces-redirect=true";
	}
	
	/*
	 * Displays selected product details
	 */
	public String viewProduct(long id) throws IOException {
		product = products.getProductById(id);
		if (product != null) {
			System.out.println("viewProduct => " + product.getProductId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getRequestMap().put("Product", product);
		} else {
			System.out.println("Product not found");
		}
		
		return "/Product/ProductDetails.xhtml?faces-redirect=true";
	}
	
	/*
	 * Sets list of products based on search string
	 */
	public void viewSearchProducts(String searchProduct) {
		if(searchProduct == null || searchProduct == "" || searchProduct.length() == 0) {
			searchProducts = null;
		} else {
			searchProducts = products.getProductsBySearchString(searchProduct);
		}
	}
	
	/*
	 * Updates products in database
	 */
	public String updateProduct(Product product) {
		products.saveProduct(product);
		return "/Admin/Product/ProductListing.xhtml?faces-redirect=true";
	}
	
	/*
	 * Removes products database
	 */
	public void removeProduct(long id) throws IOException {
		Product product = products.getProductById(id);
		if (product != null) {
			products.removeProduct(product);
		}
		FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets/Admin/Product/ProductListing.xhtml?faces-redirect=true");
	}
	
	public void goToProductListing() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets/Admin/Product/ProductListing.xhtml?faces-redirect=true");
	}
	
	public void goToAddNewProduct() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets/Admin/Product/AddProduct.xhtml?faces-redirect=true");
	}
	
	/* Shopping cart logic */
	public String checkout() {
		return "/Orders/Cart.xhtml?faces-redirect=true";
	}
	
	/* Finish the order */
	public String completeOrder() {
		shoppingCart.clearShoppingCart();
		return "/Orders/OrderComplete.xhtml?faces-redirect=true";
	}
	
	/* add to shopping cart */
	public String addProductToCart(Product p) {
		shoppingCart.addProduct(p);
		return "/Product/SearchProduct.xhtml?faces-redirect=true";
	}
	
	/* remove from shopping cart */
	public String removeProductToCart(Product p) {
		shoppingCart.removeProduct(p);
		return "/Ordres/Cart.xhtml?faces-redirect=true";
	}
	
	/* remove from shopping cart */
	public String clearShoppingCart() {
		shoppingCart.clearShoppingCart();
		return "/Product/SearchProduct.xhtml?faces-redirect=true";
	}
	
	/* get total price of cart */
	public float getShoppingCartTotal() {
		return shoppingCart.getTotalCost();
	}
}

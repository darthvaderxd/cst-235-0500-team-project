/*
	Created By: Matthew McSpadden
	Created: 11/22/2020
	Last Update Time: 11/29/2020
	Last Updated by: Donald Trowbridge
	Notes:
		-11/26/2020 : Updated this controller to use the EJB UserBusinessService.
*/


package controllers;

import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import business.UserBusinessService;
import entities.User;



// CST-235 Milestone Project | Matthew McSpadden
@ManagedBean(name = "registrationForm")
public class RegisterController {
	
	private User user;
	@EJB protected UserBusinessService service;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String register(User user) {
		if (user != null && service.addUser(user)) {
			//getting values in newUser and setting them here
			FacesContext context = FacesContext.getCurrentInstance();
			User newUser = context.getApplication().evaluateExpressionGet(context, "#{User}", User.class);
			
			//post to NewUserService to create newUser
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("User", newUser);
			
			//let them know they have reg with this html page
			return "RegConfirmed.xhtml";
		} else {
			return "Registration.xhtml?messages=Error%20creating%20user%20username%20in%20use%20or%20another%20error%20try%20again";
		}
	}
	
	public void goToLogin() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets");
    }
}
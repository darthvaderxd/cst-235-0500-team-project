/*
	Created By: Donald Trowbridge
	Created: 11/21/2020
	Last Update Time: 12/18/2020
	Last Updated by: Donald Trowbridge
	Notes:
		-11/21/2020 : As it stands, this controller is considered complete.
		-11/26/2020 : Updated this controller to use the EJB UserBusinessService. Renamed this file from Login to LoginController
		-12/18/2020 : Updated logoff method to invalidate current session.
*/


package controllers;

import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import business.UserBusinessService;
import entities.User;
import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.ejb.EJB;

@ManagedBean(name="Login")
@SessionScoped
public class LoginController {

	protected User user;
	
	@EJB
	protected UserBusinessService service;
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	/*
	* Compares the user object passed from the login page to the list of users and authenticates
	* user if login successful.
	*/
	public String onLogin(User user) {	
		
		/*
		*Checks if user name exists and grabs actual user object if match is found	
		*/
		User actualUser = service.findUserByUserName(user.getUsername());
		
		/*
		* If user is valid sets the user for the Login controller, adds the user object to the request map, and redirects to product page.
		* else redirects back to the user login
		*/
		if(service.validateLogin(user.getUsername(), user.getPassword())) {
			this.setUser(actualUser);
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getRequestMap().put("User", actualUser);
			return "/Product/SearchProduct.xhtml";
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Login or password invalid"));
			return "Login.xhtml";			
		}
	}
	
	/*
	* Logs user out of site.
	*/
	public void onLogoff() throws IOException {
		
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		
		/*FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getRequestMap().remove("User", this.getUser());
		
		this.setUser(null);*/
		
		FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets/Login/Login.xhtml");
	}
	
	/*
	* Directs user to registration page
	*/
	public void goToRegistration() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets/Login/Registration.xhtml?faces-redirect=true");
	}
	
	public UserBusinessService getService() {
		return service;
	}
	
	public void setService(UserBusinessService service) {
		this.service = service;
	}
	
	public String getGreeting() {
		String greeting = "Hello ";
		User user = (User) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("User");
		if(user == null) {
			greeting += "customer";
		}else {
			greeting += user.getFirstName();
		}
		return greeting;
	}
}

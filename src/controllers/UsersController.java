/*
	Created By: Donald Trowbridge
	Created: 12/18/2020
	Last Update Time:
	Last Updated by:
	Notes:
*/


package controllers;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.ejb.EJB;
import dao.UserDAO;

import java.io.IOException;
import java.util.List;
import entities.User;

/**
 * Session Bean implementation class UsersController
 */
@Stateless
@LocalBean
@ManagedBean
@SessionScoped
public class UsersController {
	@EJB UserDAO users;
	protected User user;
	
    public List<User> getAllUsers(){
    	return users.getUsers();
    }
    
    public User getUser() {
    	return this.user;
    }
    
    public void setUser(User user) {
    	this.user = user;
    }
    
    public void editUser(User user) throws IOException {
    	setUser(user);
    	FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets/Admin/Profiles/EditProfile.xhtml?faces-redirect=true");
    }
  
    public void updateUser(User user) throws IOException {
    	   	users.saveUser(this.user);
	    	FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets/Admin/Profiles/ProfileAdmin.xhtml?faces-redirect=true");
    }
    
    public void deleteUser(User user) throws IOException {
    	users.deleteUser(user);
    	FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets/Admin/Profiles/ProfileAdmin.xhtml?faces-redirect=true");
    }
    
    public void cancelUpdate() throws IOException {
    	FacesContext.getCurrentInstance().getExternalContext().redirect("/cat-widgets/Admin/Profiles/ProfileAdmin.xhtml?faces-redirect=true");
    }
}

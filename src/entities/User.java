/*
	Created By: Richard Williams
	Created: 11/6/2020
	Last Update Time:
	Last Updated by:
	Notes:
		11/6/2020 : Created User Entity
*/


package entities;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

import com.sun.istack.internal.NotNull;

@Entity()
@XmlRootElement
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = "user_id"))
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
@ManagedBean(name = "User")
@SessionScoped
public class User {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "user_id", unique=true, nullable = false)
	private long userId;
	
	@NotNull
	@NotEmpty
	@Column(name="username")
	protected String username;
	
	@NotNull
	@NotEmpty
	@Column(name="email")
	protected String email;
	
	@NotNull
	@NotEmpty
	@Column(name="first_name")
	protected String firstName;
	
	@NotNull
	@NotEmpty
	@Column(name="last_name")
	protected String lastName;
	
	@NotNull
	@NotEmpty
	@Column(name="password")
	protected String password;
	
	public User() {}
	
	public User(String username, String email, String firstName, String lastName, String password) {
		this.username = username;
		this.password = password;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}

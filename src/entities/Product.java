/*
	Created By: Richard Williams
	Created: 11/26/2020
	Last Update Time:
	Last Updated by:
	Notes:
		11/6/2020 : Created Product Entity
*/

package entities;

import javax.faces.bean.ManagedBean;

// Written by: Richard Williamson

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

import com.sun.istack.internal.NotNull;

@Entity
@XmlRootElement
@Table(name = "product", uniqueConstraints = @UniqueConstraint(columnNames = "product_id"))
@NamedQuery(name="Product.findAll", query="SELECT p FROM Product p")
@ManagedBean(name = "Product")
public class Product {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "product_id", unique=true, nullable = false)
	private long productId;
	
	@NotNull
	@NotEmpty
	@Column(name="product_name")
	protected String productName;
	
	@NotNull
	@NotEmpty
	@Column(name="product_description", columnDefinition="TEXT")
	protected String description;
	
	@NotNull
	@Column(name="cost", columnDefinition="FLOAT")
	protected float cost;

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
}

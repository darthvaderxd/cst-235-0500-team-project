package entities;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

import com.sun.istack.internal.NotNull;

@Entity()
@XmlRootElement
@Table(name = "roles", uniqueConstraints = @UniqueConstraint(columnNames = "role_id"))
@ManagedBean(name = "Role")
@SessionScoped
public class Role {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "role_id", unique=true, nullable = false)
	private long roleId;
	
	@NotNull
	@NotEmpty
	@Column(name="username")
	protected String username;
	
	@NotNull
	@NotEmpty
	@Column(name="role")
	protected String role;

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
